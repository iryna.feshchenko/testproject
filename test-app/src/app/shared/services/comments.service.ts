import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentModel } from '../models/comments.model';


const httpOptions = {
  headers: new HttpHeaders ({
    'Content-Type': 'application/json; charset=UTF-8',
  })
};

@Injectable()
export class CommentsService {
  private postsUrl = 'https://jsonplaceholder.typicode.com/posts';
  constructor(
    private http: HttpClient,
  ) { }

  getPostsComments(id): Observable<CommentModel[]> {
    return this.http.get<CommentModel[]>(`${this.postsUrl}/${id}/comments`);
  }
}
