import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private postsUrl = 'https://jsonplaceholder.typicode.com/users';
  constructor(
    private http: HttpClient,
  ) { }

  getUserInfo(): Observable<UserModel> {
    return this.http.get<UserModel>(this.postsUrl);
  }
}
