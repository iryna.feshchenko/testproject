import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'Angular Form Validation Tutorial';
  angForm: FormGroup;
  array: any;

  constructor(private fb: FormBuilder, private toastr: ToastrService, private router: Router) {
    this.createForm();
  }
   createForm(): void {
    this.angForm = this.fb.group({
       login: ['', Validators.required ],
       password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.array = JSON.parse(localStorage.getItem('userInfo'));
    if ( this.array !== null && this.array.login === 'admin@admin.com' && this.array.password === 'admin'){
      this.router.navigate([`home`]);
    }
  }

  updateName(): void {
    if (this._login.status === 'VALID' && this._password.status === 'VALID') {
      if (this._login.value === 'admin@admin.com' && this._password.value === 'admin'){
        this.toastr.success('Hello world!', 'Toastr fun!');
        const user = {
          login: this._login.value,
          password: this._password.value,
        };
        localStorage.setItem('userInfo', JSON.stringify(user));
        this.router.navigate([`home`]);
      }else{
        this.toastr.error('Failed login', '123');
      }
    }else{
      this.toastr.error('Failed login', '3');
    }
  }

  // tslint:disable-next-line:typedef
  get _login() {
    return this.angForm.get('login');
  }

  // tslint:disable-next-line:typedef
  get _password() {
    return this.angForm.get('password');
  }
}
