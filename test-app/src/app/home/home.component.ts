import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  login = JSON.parse(localStorage.getItem('userInfo')).login;
  Clock = Date.now();
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['posts'], {relativeTo: this.route});
    setInterval(() => {
      this.Clock = Date.now();
    }, 1000);
  }
  logOut(): any{
    localStorage.removeItem('userInfo');
    this.router.navigate([`login`]);
  }

}
