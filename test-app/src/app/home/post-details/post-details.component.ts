import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommentModel } from 'src/app/shared/models/comments.model';
import { PostModel } from 'src/app/shared/models/post/post.model';
import { UserModel } from 'src/app/shared/models/user.model';
import { CommentsService } from 'src/app/shared/services/comments.service';
import { PostsService } from 'src/app/shared/services/posts.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit, OnChanges {
  post: any = [];
  comments: CommentModel[];
  users: any = [];
  id: number;
  length: number;

  constructor(private postsService: PostsService,
              private commentsService: CommentsService,
              private usersService: UserService,
              private activateRoute: ActivatedRoute) {
    this.id = activateRoute.snapshot.params['id'];
   }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    console.log(this.id);
    this.getPosts(this.id);
  }

  getPosts(id): any {
    this.postsService.getPost(id)
      .subscribe( posts => {
        this.post = posts;
       } );
    this.commentsService.getPostsComments(id)
       .subscribe(comments => {
         this.comments = comments;
         this.setInfoComments();
         this.length = this.comments.length;
       });
    this.usersService.getUserInfo()
       .subscribe( users => {this.users = users; this.setUserName(); });
  }

  setUserName(): any {
    if (this.post && this.users) {
        for (const user of this.users) {
          if (this.post.userId === user.id) {
            this.post.name = user.name;
            this.post.email = user.email;
          }
      }
    }
  }
  setInfoComments(): any{
    if (this.comments && this.users) {
      for (const post of this.comments) {
        for (const user of this.users) {
          if (post.id === user.id) {
            post.name = user.name;
            post.email = user.email;
          }
        }
      }
    }
  }
}
