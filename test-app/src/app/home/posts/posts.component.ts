import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostModel } from 'src/app/shared/models/post/post.model';
import { PostsService } from 'src/app/shared/services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  private loading = false;
  posts: PostModel[];
  postA: any;
  config: any;

  constructor(private postsService: PostsService, private route: ActivatedRoute) {
    this.getPosts();
   }

  ngOnInit(): void {
  }
  getPosts(): any {
    this.postsService.getPosts()
      .subscribe( posts => {
        this.posts = posts;
        this.config = {
          itemsPerPage: 5,
          currentPage: 1,
          totalItems: this.posts.length
        };
      });
  }
  openPost(post): any{
    console.log(post);
    this.postsService.getPost(post)
      .subscribe( posts => {
        this.postA = posts;
       } );
  }
  pageChanged(event): any{
    this.config.currentPage = event;
  }
}
